/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/webrequest',
  version: '3.0.29',
  description: 'securely request from browsers'
}
