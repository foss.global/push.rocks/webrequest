import { expect, tap } from '@pushrocks/tapbundle';
import * as webrequest from '../ts/index.js';

tap.test('should run multiendpoint request', async (tools) => {
  const response = await new webrequest.WebRequest().request('https://api.signup.software', {
    method: 'GET',
  });
  console.log(JSON.stringify(await response.text()));
});

tap.start();
