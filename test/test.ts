import { expect, tap } from '@pushrocks/tapbundle';
import * as webrequest from '../ts/index.js';

// test dependencies
import * as typedserver from '@apiglobal/typedserver';

let testServer: typedserver.servertools.Server;

tap.test('setup test server', async () => {
  testServer = new typedserver.servertools.Server({
    cors: false,
    forceSsl: false,
    port: 2345,
  });

  testServer.addRoute(
    '/apiroute1',
    new typedserver.servertools.Handler('GET', (req, res) => {
      res.status(429);
      res.end();
    })
  );

  testServer.addRoute(
    '/apiroute2',
    new typedserver.servertools.Handler('GET', (req, res) => {
      res.status(500);
      res.end();
    })
  );

  testServer.addRoute(
    '/apiroute3',
    new typedserver.servertools.Handler('GET', (req, res) => {
      res.status(200);
      res.send({
        hithere: 'hi',
      });
    })
  );

  await testServer.start();
});

tap.test('first test', async (tools) => {
  const response = await (
    await new webrequest.WebRequest().requestMultiEndpoint(
      [
        'http://localhost:2345/apiroute1',
        'http://localhost:2345/apiroute2',
        'http://localhost:2345/apiroute4',
        'http://localhost:2345/apiroute3',
      ],
      {
        method: 'GET',
      }
    )
  ).json();

  const response2 = await new webrequest.WebRequest().getJson('http://localhost:2345/apiroute3');

  console.log('response 1: ' + JSON.stringify(response));
  console.log('response 2: ' + JSON.stringify(response2));

  expect(response).toHaveProperty('hithere'); //.to.equal('hi');
  expect(response2).toHaveProperty('hithere'); //.to.equal('hi');
});

tap.test('should cache response', async () => {
  const webrequestInstance = new webrequest.WebRequest();
  const response = await webrequestInstance.getJson('http://localhost:2345/apiroute3', true);
  expect(response).toHaveProperty('hithere');
  await testServer.stop();
  const response2 = await webrequestInstance.getJson('http://localhost:2345/apiroute3', true);
  expect(response2).toHaveProperty('hithere');
});

tap.start();
